<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form SanberBook</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First name: </label><br><br>
        <input type="text" id="fname" name="fname"><br><br>
        <label for="lname">Last name: </label><br><br>
        <input type="text" id="lname" name="lname"><br><br>
        <label for="gender">Gender</label><br><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br><br>
        <label for="nation">Nationality</label>
        <select name="nation" id="nation">
            <option value="indonesia">Indonesian</option>
            <option value="singapura">Singapura</option>
            <option value="malaysia">Malaysia</option>
            <option value="thailand">Thailand</option>
        </select><br><br>
        <label for="language">Language Spoken: </label><br><br>
        <input type="checkbox" id="language1" name="language1" value="indonesia">
        <label for="language1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="language2" name="language2" value="english">
        <label for="language2">English</label><br>
        <input type="checkbox" id="language3" name="language3" value="arabic">
        <label for="language3">Arabic</label><br>
        <input type="checkbox" id="language4" name="language4" value="japanese">
        <label for="language4">Japanese</label><br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" rows="10" cols="40"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>